/*

*/

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

var config = {
     data: {
   datasets: []
 },
  options: {
   title: {
     display: true,
     text: 'Chart.js Scatter Chart'
   },
   scales: {
     xAxes: [{
       position: 'bottom',
       gridLines: {
         zeroLineColor: "rgba(0,255,0,1)"
       },
       scaleLabel: {
         display: true,
         labelString: 'x axis'
       }
     }],
     yAxes: [{
       position: 'left',
       gridLines: {
         zeroLineColor: "rgba(0,255,0,1)"
       },
       scaleLabel: {
         display: true,
         labelString: '\u03B5(L mol^-1 cm^-1)'
       }
     }]
   }
  }
};

function create_chart(){
var ctx = document.getElementById('canvas').getContext('2d');
window.myChart = new Chart.Scatter(ctx, config);
}

