/*

*/
function init_sliders(){
    $('.std-slider').nstSlider({
        "left_grip_selector": ".leftGrip",
        "value_changed_callback": function(cause, leftValue, rightValue) {
            $(this).siblings('.labels').text('std : ' + leftValue/10);
        }
    });
  $('.xaxis-slider').nstSlider({
        "left_grip_selector": ".leftGrip",
        "right_grip_selector": ".rightGrip",
        "value_bar_selector": ".bar",
        "value_changed_callback": function(cause, leftValue, rightValue) {
            $(this).siblings('.labels').text(leftValue/10 + ' - ' + rightValue/10 + ' eV');
        }
    });

}