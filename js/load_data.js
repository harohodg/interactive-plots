/*

*/
function loadFile(filePath) {
  var result = null;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("GET", filePath, false);
  xmlhttp.send();
  if (xmlhttp.status==200) {
    result = xmlhttp.responseText;
  }
  return result;
}

function import_file(filePath){
  var file_data;
  var file_data = loadFile(filePath);
  return Papa.parse(file_data, {
    header: true,
    delimiter:'|',
    dynamicTyping:true,
    skipEmptyLines:true,
  })
}


function load_spectra(file_names, root){
  var spectra_data = [];

  $.each(file_names, function(file_index, file_name){
    spectra_data[file_index] = import_file(root.concat(file_name)).data;
  })
  return spectra_data;
}

function add_checkboxes(file_names, target){
  $.each(file_names, function(index, file_name){
    $('#' + target).append('<label><input type="checkbox" checked /> ' + file_name + '</label><br/>');
  })
}