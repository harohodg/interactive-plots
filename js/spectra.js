/*

*/

function add_data_set(data, label){
  var colorNames = Object.keys(window.chartColors);
  var colorName = colorNames[config.data.datasets.length % colorNames.length];
  var newColor = window.chartColors[colorName];
  var newDataset = {
    label: label,
    backgroundColor: newColor,
    borderColor: newColor,
    data: data,
    fill: false,
    pointRadius:0}

config.data.datasets.push(newDataset);
}

function get_points(data, xaxis, yaxis){
  return $.map(data, function(d){ return {x:d[xaxis], y:d[yaxis]} })
}
// -----------------------------------------------------
function get_xvals(xmin, xmax, num_x){
  var m = (xmax - xmin)/(num_x - 1);
  var b = xmin - m;
  return $.map( Array.from({length:num_x},(v,k)=>k+1), function(i){ return m * i + b; })

}


function get_impulses(data){
  var new_data = [];
  $.each(data, function(i, point){
    $.merge(new_data, [{x:point.x, y:0 }, point, {x:point.x, y:0 }])
  });
  return new_data;
}

function gaussian(xval, mean, std, scale){
  scale = (scale == null ? 1 : scale)
  var numerator = xval - mean;
  var denominator = std;
  return scale*Math.exp( -1*Math.pow(numerator/denominator, 2) )
}

function epsilon_scale(y, std){
  const s = 1.6196016 * Math.pow(10,4);
  return s * y / std;
}

function delta_epsilon_scale(x, y, std){
  const s = 0.02456756;
  return s * y * x / std;
}

function sum_gaussians(xval, std, points, plot_type, xmin, xmax, abs){
  var s     = 0;
  var scale = 0;
  var si;
  var px, py;

  for (var i = 0; i < points.length; i++) {
    px = points[i].x;
    py = (abs == true ? Math.abs(points[i].y) : points[i].y);
    scale = (plot_type == 'AB' ? epsilon_scale(py, std) : delta_epsilon_scale(px, py, std) );
    si = (px >= xmin && px <= xmax ?  gaussian(xval, px, std, scale) : 0);
    s += si;
  }
  return s;
}



function get_gaussian(config, points){
  var xmin  = config.xmin;
  var xmax  = config.xmax;
  var num_x = config.num_x;

  var plot_type = config.plot_type;
  var plot_xmin = config.plot_xmin;
  var plot_xmax = config.plot_xmax;
  var plot_abs  = config.plot_abs;

  var std  = config.std;

  var x_vals = get_xvals(xmin, xmax, num_x);

  return $.map(x_vals, function(xval){return {x:xval, y:sum_gaussians(xval, std, points, plot_type, plot_xmin, plot_xmax, plot_abs)} } );

}

function plot_data(chiral_data, fake_chiral_data){
  var plot_type = $('input[name=plot-type]:checked').val();
  var plot_std  = parseFloat( $('.std-slider').nstSlider('get_current_min_value') )/10;
  var plt_xmin = parseFloat( $('.xaxis-slider').nstSlider('get_current_min_value') )/10;
  var plt_xmax = parseFloat( $('.xaxis-slider').nstSlider('get_current_max_value') )/10;
  var plt_abs  = $('#plot-abs').children('input[type=checkbox]')[0].checked;
  console.log(plot_type, plot_std, plt_xmin, plt_xmax);

  config.data.datasets = [];
  const plot_config = {xmin:0, xmax:5, num_x:50, std: plot_std, plot_type:plot_type, plot_xmin:plt_xmin, plot_xmax:plt_xmax, plot_abs:plt_abs};
  $.each( $('#chiral-files').children('label'), function(index, label){
    var data = chiral_data[index];
    var plot_label = $(label).text().replace(/_/g, " ").replace(" Spectra", "");

    if ( $(label).children('input[type=checkbox]')[0].checked ){
      add_data_set(get_gaussian( plot_config, get_points(data, 'eV', plot_type) ), plot_label);
    }
  })

  $.each( $('#fake-chiral-files').children('label'), function(index, label){
    var data = fake_chiral_data[index];
    var plot_label = $(label).text().replace(/_/g, " ").replace(" Spectra", "");

    if ( $(label).children('input[type=checkbox]')[0].checked ){
      add_data_set(get_gaussian( plot_config, get_points(data, 'eV', plot_type) ), plot_label);
    }
  })
  window.myChart.update();
}